import React from "react";
import UserTable from "./tables/UserTable";
import AddUserForm from "./tables/AddUserForm";
import EditUserForm from "./tables/EditUserForm";

const App = () => {
  const usersData = [
    { id: 1, name: "Tania", username: "floppydiskette" },
    { id: 2, name: "Craig", username: "siliconeidolon" },
    { id: 3, name: "Ben", username: "benisphere" }
  ];
  const [users, setUsers] = React.useState(usersData);
  const [editId, setEditId] = React.useState(undefined);
  const handleEditing = id => {
    setEditId(id);
  };

  const handleAddUser = user => {
    setUsers([...users, user]);
  };

  const handleDeleteUser = id => {
    console.log(id);
    setUsers(users.filter(user => user.id !== id));
  };

  const handleEditUser = updatedUser => {
    setUsers(
      users.map(user => (user.id !== updatedUser.id ? user : updatedUser))
    );
  };

  return (
    <div className="container">
      <h1>CRUD App with Hooks</h1>
      <div className="flex-row">
        <div className="flex-large">
          {editId === undefined ? (
            <>
              <h2>Add user</h2>
              <AddUserForm onAddUser={handleAddUser} />
            </>
          ) : (
            <>
              <h2>Edit User</h2>
              <EditUserForm
                onEditUser={handleEditUser}
                user={users.find(user => user.id === editId)}
              />
            </>
          )}
        </div>
        <div className="flex-large">
          <h2>View users</h2>
          <UserTable
            users={users}
            onDeleteUser={handleDeleteUser}
            onEdit={handleEditing}
          />
        </div>
      </div>
    </div>
  );
};

export default App;
