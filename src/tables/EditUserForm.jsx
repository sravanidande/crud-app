import React from "react";

const EditUserForm = ({ user, onEditUser }) => {
  const [name, setName] = React.useState(user.name);
  const [username, setUsername] = React.useState(user.username);

  const handleNameChange = evt => {
    setName(evt.target.value);
  };
  const handleUsernameChange = evt => {
    setUsername(evt.target.value);
  };
  const handleSubmit = evt => {
    evt.preventDefault();
    onEditUser({ id: user.id, name: name, username: username });
  };
  return (
    <form onSubmit={handleSubmit}>
      <label>Name</label>
      <input type="text" name="name" value={name} onChange={handleNameChange} />
      <label>Username</label>
      <input
        type="text"
        name="username"
        value={username}
        onChange={handleUsernameChange}
      />
      <button>Edit user</button>
      <pre>{JSON.stringify({ name, username }, null, 2)}</pre>
    </form>
  );
};

export default EditUserForm;
