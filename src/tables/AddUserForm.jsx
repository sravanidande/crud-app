import React, { useState } from 'react';

const AddUserForm = props => {
  const [name, setName] = useState('');
  const [username, setUsername] = useState('');

  const handleNameChange = evt => {
    setName(evt.target.value);
  };
  const handleUsernameChange = evt => {
    setUsername(evt.target.value);
  };
  const handleSubmit = evt => {
    evt.preventDefault();
    props.onAddUser({ name: name, username: username });
  };
  return (
    <form onSubmit={handleSubmit}>
      <label>Name</label>
      <input type="text" name="name" value={name} onChange={handleNameChange} />
      <label>Username</label>
      <input
        type="text"
        name="username"
        value={username}
        onChange={handleUsernameChange}
      />
      <button>Add new user</button>
      <pre>{JSON.stringify({ name, username }, null, 2)}</pre>
    </form>
  );
};

export default AddUserForm;
