import React from "react";

const UserTable = props => (
  <table>
    <thead>
      <tr>
        <th>Name</th>
        <th>Username</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {props.users.map(({ id, name, username }, index) => (
        <tr key={index}>
          <td>{id}</td>
          <td>{name}</td>
          <td>{username}</td>
          <td>
            <button
              className="button muted-button"
              onClick={() => props.onEdit(id)}
            >
              Edit
            </button>
            <button
              className="button muted-button"
              onClick={() => props.onDeleteUser(id)}
            >
              Delete
            </button>
          </td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default UserTable;
